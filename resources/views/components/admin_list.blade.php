@extends('layout.main')
@section('title', 'Contact Success')
@section('content')
<form>
    <div class="form-group">
        <h3>CONTACT LIST</h3>
        <hr>
    </div>
    <div class="form-group">
        <div class="form-row d-flex justify-content-end" style="margin-right: 0.5px">
            <button class="btn btn-outline-secondary" type="button" id="email" data-toggle="modal" data-target="#myModal"><span style="color: red">BULK EMAILS </span><i class="fa fa-envelope-o"></i></button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>BULK EMAILS</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="to">To</label>
                            <input type="text" class="form-control" id="to">
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" rows="8" id="message"></textarea>
                        </div>
                        <div class="form-inline justify-content-between">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">CLOSE</button>
                            <a href="{{url('admin/contacts/{id}')}}"><button type="button" class="btn btn-outline-secondary">SEND</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-8 d-flex justify-content-md-start">
            <button class="btn btn-outline-primary mr-2" type="button">Bulk Actions <i class="fa fa-chevron-down"></i></button>
            <button class="btn btn-outline-primary mr-2" type="button">Apply</button>
            <button class="btn btn-outline-primary mr-2" type="button">Status <i class="fa fa-chevron-down"></i></button>
            <button class="btn btn-outline-primary" type="button">Filter</button>
        </div>
        <div class="col-4 d-flex justify-content-end">
            <input type="text" class="form-control mr-2" id="search" placeholder="Enter Keyword...">
            <button class="btn btn-outline-primary" type="button">SEARCH</button>
        </div>
    </div>
    <div class="form-group">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Date</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @for($i=1; $i<=10; $i++) @foreach($lists as $list) <tr>
                    <th scope="row">{{$list['no']}}</th>
                    <td>{{$list['name']}}</td>
                    <td>{{$list['subject']}}</td>
                    <td>{{$list['email']}}</td>
                    <td>{{$list['status']}}</td>
                    <td>{{$list['date']}}</td>
                    <td>{{$list['action']}}</td>
                    </tr>
                    @endforeach
                    @endfor </tbody>
        </table>
    </div>
    <div class="form-group row">
        <div class="col-4 d-flex justify-content-md-start">
            <p>Total items: 50</p>
        </div>
        <div class="col-8 d-flex justify-content-md-end">
            <div aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">
                                <</span> <span class="sr-only">Previous
                            </span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true">></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</form> 
@endsection