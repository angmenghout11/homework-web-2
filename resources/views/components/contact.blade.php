@extends('layout.main')

@section('title', 'Contact')

@section('content')
<form class="container">
    <div class="form-group">
        <h3>CONTACT</h3>
        <hr>
    </div>
    <div class="form-group">
        <label for="name">Name <span style = "color: red">*</span></label>
        <input type="text" class="form-control" id="name">
    </div>
    <div class="form-group">
        <div class="form-row">
            <div class="col-6">
                <label for="email">Email <span style="color: red">*</span></label>
                <input type="email" class="form-control" id="email">
            </div>
            <div class="col-6">
                <label for="phoneNum">Phone Number</label>
                <input type="text" class="form-control" id="pNum">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="subject">Subject</label>
        <input type="text" class="form-control" id="subject">
    </div>
    <div class="form-group">
        <label for="message">Message</label>
        <textarea class="form-control" rows="8" id="message"></textarea>
    </div>
    <div class="btn-next"> 
        <a href="{{url('/confirm')}}"><button type="button" class="btn btn-outline-primary" style="float: right">Next ></button></a>
    </div>
</form>
@endsection