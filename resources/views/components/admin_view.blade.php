@extends('layout.main')
@section('title', 'Contact Confirmation')
@section('content')
<form>
    <div class="form-group">
        <h3>CONTACT CONFIRMATION</h3>
        <hr>
    </div>
    <div class="form-group row d-flex justify-content-end" style="margin-right: 1px">
        <button class="btn btn-outline-secondary mr-2" type="button">Pending</button>
        <button class="btn btn-outline-secondary mr-2" type="button">Inprogress</button>
        <button class="btn btn-outline-secondary" type="button">Completed</button>
    </div>
    <div class="form-group">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th scope="row">Name</th>
                    <td>Ang Menghout</td>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <td>angmenghout11@gmail.com</td>
                </tr>
                <tr>
                    <th scope="row">Phone Number</th>
                    <td>089834723</td>
                </tr>
                <tr>
                    <th scope="row">Subject</th>
                    <td>Web and Cloud</td>
                </tr>
                <tr>
                    <th scope="row">Message</th>
                    <td>Consectetuer, penatibus. Sagittis sequi quidem sit hic mauris? Officia deleniti repellat fugiat
                        sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                        dictumst, ornare quis, doloremque, commodi irure lorem laborum suscipit, mattis feugiat
                        accusantium etiam dignissimos reiciendis officia autem ullamco deleniti eaque sunt irure
                        accumsan rutrum lorem parturient phasellus, commodi magni. Dolore unde ipsum est quis, ac,

                        dictumst elementum, eaque est dictumst cum! Nulla. Minim tempore bibendum laudantium
                        placeat nemo dignissimos, inventore quaerat officia quisquam auctor nisl? Aliqua facilisis.
                        Pharetra, ornare expedita error. Esse cras libero eget velit ullam, tristique eveniet diamlorem
                        nullam quos! Nemo curae minim? Nemo class. Consectetuer, penatibus. Sagittis sequi quidem

                        sit hic mauris? Officia deleniti repellat fugiat
                        sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                        dictumst, ornare quis, doloremque, commodi irure lorem
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <a href="{{url('admin/contacts')}}"><button type="button" class="btn btn-outline-primary">< BACK</button></a>
        <button class="btn btn-outline-primary" type="button" id="email" data-toggle="modal" data-target="#myModal" style="float: right">Reply</button>
        <!-- Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>EMAILS</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="to">To</label>
                            <input type="text" class="form-control" id="to">
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" rows="8" id="message"></textarea>
                        </div>
                        <div class="form-inline justify-content-between">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">CLOSE</button>
                            <a href="{{url('admin/contacts')}}"><button type="button" class="btn btn-outline-secondary">SEND</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection