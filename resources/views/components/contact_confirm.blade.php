@extends('layout.main')

@section('title', 'Contact Confirmation')

@section('content')
<form>
    <div class="form-group">
        <h3>CONTACT CONFIRMATION</h3>
        <hr>
    </div>
    <div class="form-group">
        <table class="table table-bordered">
            <tbody id="table1">
                <tr>
                    <th scope="row">Name</th>
                    <td>Ang Menghout</td>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <td>angmenghout11@gmail.com</td>
                </tr>
                <tr>
                    <th scope="row">Phone Number</th>
                    <td>089824723</td>
                </tr>
                <tr>
                    <th scope="row">Subject</th>
                    <td>Web and Cloud</td>
                </tr>
                <tr>
                    <th scope="row">Message</th>
                    <td>Consectetuer, penatibus. Sagittis sequi quidem sit hic mauris? Officia deleniti repellat fugiat
                        sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                        dictumst, ornare quis, doloremque, commodi irure lorem laborum suscipit, mattis feugiat
                        accusantium etiam dignissimos reiciendis officia autem ullamco deleniti eaque sunt irure
                        accumsan rutrum lorem parturient phasellus, commodi magni. Dolore unde ipsum est quis, ac,

                        dictumst elementum, eaque est dictumst cum! Nulla. Minim tempore bibendum laudantium
                        placeat nemo dignissimos, inventore quaerat officia quisquam auctor nisl? Aliqua facilisis.
                        Pharetra, ornare expedita error. Esse cras libero eget velit ullam, tristique eveniet diamlorem
                        nullam quos! Nemo curae minim? Nemo class. Consectetuer, penatibus. Sagittis sequi quidem

                        sit hic mauris? Officia deleniti repellat fugiat
                        sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                        dictumst, ornare quis, doloremque, commodi irure lorem
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group row d-flex justify-content-between" style="margin-right: 1px; margin-left: 1px">
        <a href="{{url('contact')}}"><button type="button" class="btn btn-outline-primary">< Back</button></a>
        <a href="{{url('/success')}}"><button type="button" class="btn btn-outline-primary">Submit ></button></a>
    </div>
</form>
@endsection