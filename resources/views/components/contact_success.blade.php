@extends('layout.main')

@section('title', 'Contact Success')

@section('content')
<form>
    <div class="form-group">
        <h3>CONTACT SUBMITED</h3>
        <hr>
    </div>
    <div class="form-group" style="text-align: center; margin-top: 100px">
        <h4>You message have been submit to our system!
        </h4>
        <p>Our team will feedback to you soon, Thank you</p>
    </div>
    <div class="form-group col-md-12 text-center" style="margin-top: 100px">
        <a href="{{url('contact')}}"><button type="button" class="btn btn-outline-primary">< Go Back Home Page</button></a> 
    </div> 
</form> 
@endsection