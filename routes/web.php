<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/contact', 'ContactController@contact');
Route::get('/confirm', 'ContactController@contact_confirm');
Route::get('/success', 'ContactController@contact_success');
Route::get('/admin/contacts', 'AdminController@admin_list');
Route::get('/admin/contacts/{id}', 'AdminController@admin_view');
