<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{

    protected $lists;

    public function admin_list()
    {
        $this->lists = [
            [
                'no' => '#1',
                'name' => 'Ang Menghout',
                'subject' => 'Web and Cloud',
                'email' => 'angmenghout11@gmail.com',
                'status' => 'panding',
                'date' => '03/29/2020',
                'action' => 'Edit | '
            ]
        ];

        return view('components.admin_list')->with('lists', $this->lists);
    }

    public function admin_view()
    {
        return view('components.admin_view');
    }
}
