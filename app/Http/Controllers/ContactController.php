<?php

namespace App\Http\Controllers;

class ContactController extends Controller
{
    public function contact()
    {
        return view('components.contact');
    }

    public function contact_confirm()
    {
        return view('components.contact_confirm');
    }

    public function contact_success()
    {
        return view('components.contact_success');
    }
}
